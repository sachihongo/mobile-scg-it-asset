import { Component} from '@angular/core';
import {
  BarcodeScannerOptions,
  BarcodeScanner
} from "@ionic-native/barcode-scanner/ngx";
import { AlertController, Platform, MenuController} from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage{
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;
  backButtonSubscription; 

  constructor(private barcodeScanner: BarcodeScanner, public alertController: AlertController, private platform: Platform, public menuCtrl: MenuController) {

    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
    // this.platform.backButton.subscribe(() => {
    //   navigator['app'].exitApp();
    // });
    // this.scanCode();
  }
  
  scanCode() {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        window.open("http://" + barcodeData.text, '_system', 'location=yes');
        // this.presentAlert(barcodeData);
        // this.scannedData = barcodeData;
      })
      .catch(err => {
        console.log("Error", err);
      });
  }

  async presentAlert(data) {
    const alert = await this.alertController.create({
      message: data.text,
      buttons: [{
        text: 'Ok',
        handler: () => {
          window.open("http://" + data.text, '_system', 'location=yes');
        }
      }]
    });
    await alert.present();
  }

  exit(){
    navigator['app'].exitApp();
  }

}
